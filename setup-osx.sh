#! /usr/local/bin/fish

mkdir -p ~/.config/fish/functions
cp ./functions/* ~/.config/fish/functions
set -U editor nano
