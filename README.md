# Escalate Fish

A function for functionally functioning without knowing the write-status of a file. Otherwise know as, *I can't remember to ```sudo nano``` when it matters* or how I learned to stop worrying and use a fish function.

Simple ```setup.sh``` and go! 
(Or ```setup-osx.sh``` if you use fruity products)

## What's going to happen when I run your above shell script

Well it will copy the function to your fish function folder. It will also set a universal variable called ```editor``` which defaults to ```nano```.

You can change this by running ```set editor my_favourite_editor``` where ```my_favourite_editor``` is something like nano, vim, sublime, whatever.

## Usage

Instead of something like ```nano some_file```, you can:  
```escalate some_file``` 

If that file requires priviledge escalation, you'll be prompted for it.


