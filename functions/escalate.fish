function escalate --description 'A simple function to help you edit config files or text files without having to remember to use sudo'
       	if test -w $argv
		echo "Cleared for takeoff..."
		eval $editor $argv
	else
		echo "This file is write protected..."
		sudo $editor $argv
	end
end
